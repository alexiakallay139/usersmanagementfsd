import { ImageRecognitionController } from "./controller/ImageRecognitionController";
import { UserController } from "./controller/UserController";

const authMiddleware = require("./middleware/Auth");

export const Routes = [{
    method: "get",
    route: "/users",
    middleware: authMiddleware,
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    middleware: authMiddleware,
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/users",
    middleware: authMiddleware,
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    middleware: authMiddleware,
    controller: UserController,
    action: "remove"
}, {
    method: "put",
    route: "/users/:id",
    middleware: authMiddleware,
    controller: UserController,
    action: "update"
}, {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login"
}, {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register"
}, {
    method: "get",
    route: "/train",
    controller: ImageRecognitionController,
    action: "train"
}, {
    method: "post",
    route: "/evaluate",
    controller: ImageRecognitionController,
    action: "evaluate"
}];