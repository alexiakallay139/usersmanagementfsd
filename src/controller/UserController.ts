import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

export class UserController {
    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return await this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        const results = await this.userRepository.findOne(request.params.id);
        return response.send(results);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let userToUpdate = await this.userRepository.findOne(request.params.id);

        const { email, userName = "my user name", password } = request.body;

        const user = this.userRepository.create(request.body);
        const result = await this.userRepository.update(request.params.id, request.body);
        // return response.send({ ...userToUpdate, ...request.body });
        return response.send(result);
    }

    async register(request: Request, response: Response, next: NextFunction) {
        try {
            const { userName, password, email } = request.body;

            if (!(email && password && userName)) {
                return response.status(400).send("All input is required.");
            }

            const oldUser = await this.userRepository.findOne({ userName, email });

            if (typeof oldUser !== "undefined") {
                return response.status(409).send("User already exists. Please login.");
            }

            const encryptedPassword = await bcrypt.hash(password, 10);

            const user = await this.userRepository.create({
                userName,
                email: email.toLowerCase(),
                password: encryptedPassword,
            });

            await this.userRepository.save(user).then(() => {
                return response.status(201).send("Successful register!");
            });
        } catch (err) {
            console.log(err);
        }
    }

    async login(request: Request, response: Response, next: NextFunction) {
        try {
            const { email, password } = request.body;

            if (!(email && password)) {
                return response.status(400).send("All input is required.");
            }

            const user = await this.userRepository.findOne({ email });

            if (user && (await bcrypt.compare(password, user.password))) {
                const token = jwt.sign(
                    { user_id: user.id, email },
                    process.env.REACT_APP_TOKEN_KEY,
                    {
                        expiresIn: "2h",
                    }
                );
                user.token = token;
                return response.type("json").status(200).send(JSON.stringify(user));
            } else {
                return response.status(400).send("Invalid credentials.");
            }
        } catch (err) {
            console.log(err);
        }
    }
}