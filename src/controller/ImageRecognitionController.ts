import { MnistData } from "../image-recognition/data.js";
import { getModel, train, doPredictionOnOneImage } from "../image-recognition/script";
import { toArrayBuffer } from "../image-recognition/utils.js";
import * as tf from "@tensorflow/tfjs-node";

export class ImageRecognitionController {
    async train(request, response) {
        try {
            const data = new MnistData();
            var model = getModel();

            await data.load();
            const history = await train(model, data);
            await model.save("file://./model");
            return response.status(200).send(history);

        } catch (error) {
            return response.status(500).send(JSON.stringify(error));
        }
    }

    async evaluate(request, response) {
        if (!request.files) {
            return response.status(400).send("No files were uploaded.");
        }

        const model = await tf.loadLayersModel("file://./model/model.json");
        const arrBuffer = toArrayBuffer(request.files.image.data);
        var preds = await doPredictionOnOneImage(model, arrBuffer);

        return response.status(200).send(preds);
    }
}