import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";
import { User } from "./entity/User";
import * as cors from "cors";
import * as fileUpload from "express-fileupload";

require('dotenv').config();

const config: any = {
    type: "postgres",
    host: process.env.REACT_APP_DB_HOST,
    port: parseInt(process.env.REACT_APP_DB_PORT),
    username: process.env.REACT_APP_DB_USER,
    password: process.env.REACT_APP_DB_PASSWORD,
    database: process.env.REACT_APP_DATABASE,
    entities: [
        User
    ],
    synchronize: (process.env.REACT_APP_DB_SYNCHRONIZE === "true"),
    logging: (process.env.REACT_APP_DB_LOGGING === "true")
}

createConnection(config).then(() => {
    const app = express();
    app.use(bodyParser.json());
    app.use(cors({
        origin: '*'
    }));
    app.use(fileUpload());

    Routes.forEach(route => {
        if (route.middleware) {
            (app as any)[route.method](route.route, route.middleware, (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined ? res.send(result.data) : undefined);

                } else if (result !== null && result !== undefined) {
                    res.json(result.data);
                }
            })
        } else {
            (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined ? res.send(result.data) : undefined);

                } else if (result !== null && result !== undefined) {
                    res.json(result.data);
                }
            });
        }
    });

    app.set("env", process.env.REACT_APP_ENV);

    app.listen(process.env.REACT_APP_PORT);

    console.log(`Express server has started on port ${process.env.REACT_APP_PORT}. Open http://localhost:${process.env.REACT_APP_PORT}/users to see results`);

}).catch(error => console.log(error));
