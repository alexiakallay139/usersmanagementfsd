const { Datastore } = require("@google-cloud/datastore");
const moment = require("moment");

const datastore = new Datastore({
    projectId: "rising-guide-333207",
    keyFilename: "./datastore-credentials.json"
});

// Get all schedules
// LINK: https://europe-central2-rising-guide-333207.cloudfunctions.net/getSchedules

exports.getSchedules = async (req, res) => {
    res.set({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET",
        "Access-Control-Allow-Headers": "*"
    });

    const query = datastore.createQuery("Schedule");
    await datastore.runQuery(query).then((schedules) => {
        res.send(JSON.stringify(schedules[0]));
    });
};

// Add new schedule
// LINK: https://europe-central2-rising-guide-333207.cloudfunctions.net/createSchedule

exports.createSchedule = async (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");

    if (req.method == "OPTIONS") {
        res.set({
            "Access-Control-Allow-Methods": "POST",
            "Access-Control-Allow-Headers": "*"
        })
        return res.status(200).send("");
    }

    const { start_date, end_date } = req.body;

    if (!(typeof start_date === "string") || !(typeof end_date === "string")) {
        return res.status(500).send("Incorrect request body format.");
    }

    if (!start_date && !end_date) {
        return res.status(500).send("Please provide start and end dates.");
    }

    var formats = [
        "YYYY-MM-DD HH:mm:ss",
        "MM-DD-YYYY HH:mm:ss",
        "DD-MM-YYYY HH:mm:ss",
        "YYYY-MM-DD HH:mm",
        "MM-DD-YYYY HH:mm",
        "DD-MM-YYYY HH:mm",
        "MM/DD/YY",
        "MM/DD/YYYY",
        "DD/MM/YY",
        "D/MM/YY",
        "DD/MM/YYYY",
        "D/MM/YYYY",
        "MM-DD-YY",
        "MM-DD-YYYY",
        "DD-MM-YY",
        "DD-MM-YYYY",
        "YYYY-MM-DD",
        "YYYY/MM/DD",
        "YY/MM/DD"
    ];

    const momentStartDate = moment(start_date, formats, true);
    const momentEndDate = moment(end_date, formats, true);

    if (!momentStartDate.isValid()) {
        return res.status(500).send("Start date is invalid.");
    }

    if (!momentEndDate.isValid()) {
        return res.status(500).send("End date is invalid.");
    }

    if (momentEndDate.isBefore(momentStartDate)) {
        return res.status(500).send("End date cannot be before start date.");
    }

    const scheduleKey = datastore.key("Schedule");

    const schedule = {
        key: scheduleKey,
        data: {
            start_date: start_date,
            end_date: end_date
        },
    };

    await datastore.save(schedule).catch(error => {
        return res.status(500).send(error);
    }).then(() => {
        return res.status(200).send(JSON.stringify(schedule));
    });
}