FROM node:16

RUN mkdir /app
WORKDIR /app

COPY package.json /app
COPY tsconfig.json /app
COPY .env /app
COPY src /app/src

RUN npm install

EXPOSE 3000

CMD ["npm","run","dev"]